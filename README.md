realms-wiki-alpine
==================

  * A docker image for [scragg0x/realms-wki](https://github.com/scragg0x/realms-wiki) with [Alpine linux](http://alpinelinux.org/)

CONFIGURATION
-------------

### `REALMS_WIKI_CONFIG`

A json string of realms-wiki configuration.

this value is passing to `realms-wiki configure`

### `REALMS_WIKI_WORKERS`

A positive integer value of gunicorn workers for run realms-wiki instance.

LICENSE
-------

The content of this repository is under the GPLv3.

And note, the Dockerfile in this repository partial includes content of [lkwg82/h2o.docker](https://github.com/lkwg82/h2o.docker)'s Dockerfile.
[lkwg82/h2o.docker](https://github.com/lkwg82/h2o.docker)'s Dockerfile is under the [GPLv3](https://github.com/lkwg82/h2o.docker/blob/master/LICENSE).

AUTHOR
------

Naoki OKAMURA a.k.a nyarla <nyarla@thotep.net>
